﻿using System.Collections.Generic;
using System.Linq;
using Entities;
using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Repository;

namespace Services
{
    public interface IInquiryService
    {
        IEnumerable<Inquiry> GetAll(int getUserId);
        ActionResult<Inquiry> Get(int id, int getUserId);
        ActionResult<Inquiry> Update(int id, Inquiry inquiry, int userId);
        ActionResult Delete(int id, int userId);
        ActionResult<Inquiry> Add(Inquiry inquiry, int userId);
        ActionResult DeleteMessage(int inquiryId, int id, int getUserId);
        ActionResult<IEnumerable<InquiryMessage>> GetMessages(int inquiryId, int getUserId);
        ActionResult<InquiryMessage> GetMessage(int inquiryId, int id, int getUserId);
        ActionResult<InquiryMessage> AddMessage(int inquiryId, InquiryMessage userId, int getUserId);
        ActionResult<InquiryMessage> UpdateMessage(int inquiryId, int id, InquiryMessage value, int getUserId);
    }
    
    public class InquiryService : IInquiryService
    {
        private readonly AppSettings _appSettings;
        private readonly IRepository<Inquiry> _inquiryRepository;
        private readonly IRepository<User> _userRepository;

        public InquiryService(IOptions<AppSettings> appSettings, IRepository<Inquiry> inquiryRepository, IRepository<User> userRepository)
        {
            _appSettings = appSettings.Value;
            _inquiryRepository = inquiryRepository;
            _userRepository = userRepository;
        }

        private bool CanAccessInquiry(Inquiry inquiry, int userId) =>
            inquiry?.CreatedBy?.Id == userId || inquiry?.Post?.CreatedBy?.Id == userId || userId == 0;

        public IEnumerable<Inquiry> GetAll(int getUserId)
        {
            return _inquiryRepository.GetAll()
                .Where(x => CanAccessInquiry(x, getUserId));
        }

        public ActionResult<Inquiry> Get(int id, int getUserId)
        {
            var inquiry = Get(id);
            if (CanAccessInquiry(inquiry, getUserId))
            {
                return inquiry;
            }

            return new ForbidResult();
        }
        
        private Inquiry Get(int id)
        {
            return _inquiryRepository.Get(id);
        }

        public ActionResult<Inquiry> Add(Inquiry inquiry, int userId)
        {
            inquiry.CreatedBy = _userRepository.Get(userId);

            if (inquiry.CreatedBy == null)
            {
                return new UnauthorizedResult();
            }
            
            if (inquiry.CreatedBy.Id == inquiry.Post.CreatedByUserId)
            {
                return new ForbidResult("Tried to sent inquiry to himself");
            }
            
            _inquiryRepository.Add(inquiry);
            return inquiry;
        }

        public ActionResult DeleteMessage(int inquiryId, int id, int getUserId)
        {
            var dbInquiry = Get(inquiryId);
            if (CanAccessInquiry(dbInquiry, getUserId))
            {
                var message = dbInquiry.Messages.FirstOrDefault(x => x.Id == id);

                if (message != null)
                {
                    dbInquiry.Messages.Remove(message);
                    _inquiryRepository.Update(dbInquiry, dbInquiry);
                }
                
                return new OkResult();
            }

            return new ForbidResult();
        }

        public ActionResult<IEnumerable<InquiryMessage>> GetMessages(int inquiryId, int getUserId)
        {
            var dbInquiry = Get(inquiryId);
            if (CanAccessInquiry(dbInquiry, getUserId))
            {
                return dbInquiry.Messages;
            }

            return new ForbidResult();
        }

        public ActionResult<InquiryMessage> GetMessage(int inquiryId, int id, int getUserId)
        {
            var dbInquiry = Get(inquiryId);
            if (CanAccessInquiry(dbInquiry, getUserId))
            {
                var message = dbInquiry.Messages.FirstOrDefault(x => x.Id == id);

                if (message != null)
                {
                    return message;
                }

                return new NotFoundResult();
            }

            return new ForbidResult();
        }

        public ActionResult<InquiryMessage> AddMessage(int inquiryId, InquiryMessage inquiryMessage, int getUserId)
        {
            var dbInquiry = Get(inquiryId);
            if (CanAccessInquiry(dbInquiry, getUserId))
            {
                inquiryMessage.CreatedBy = _userRepository.Get(getUserId);
                dbInquiry.Messages.Add(inquiryMessage);
                _inquiryRepository.Update(dbInquiry, dbInquiry);
                return inquiryMessage;
            }

            return new ForbidResult();
        }

        public ActionResult<InquiryMessage> UpdateMessage(int inquiryId, int id, InquiryMessage value, int getUserId)
        {
            var dbInquiry = Get(inquiryId);
            if (CanAccessInquiry(dbInquiry, getUserId))
            {
                var message = dbInquiry.Messages.FirstOrDefault(x => x.Id == id);

                if (message != null)
                {
                    message = value;
                    _inquiryRepository.Update(dbInquiry, dbInquiry);
                    return message;
                }

                return new NotFoundResult();
            }

            return new ForbidResult();
        }

        public ActionResult<Inquiry> Update(int id, Inquiry inquiry, int userId)
        {
            var dbInquiry = Get(id);
            if (CanAccessInquiry(dbInquiry, userId))
            {
                _inquiryRepository.Update(dbInquiry, inquiry);
                return inquiry;
            }

            return new UnauthorizedResult();
        }

        public ActionResult Delete(int id, int userId)
        {
            var inquiry = Get(id);
            if (CanAccessInquiry(inquiry, userId))
            {
                _inquiryRepository.Delete(Get(id));
                return new OkResult();
            }

            return new ForbidResult();
        }
    }
}