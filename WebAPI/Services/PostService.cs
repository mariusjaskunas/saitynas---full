﻿using System.Collections.Generic;
using System.Linq;
using Entities;
using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Repository;

namespace Services
{
    public interface IPostService
    {
        IEnumerable<Post> GetAll(int? id);
        Post GetPost(int id);
        ActionResult<Post> Update(int id, Post user, int userId);
        void Delete(int id);
        Post Add(Post post, int userId);
        ActionResult<Comment> AddComment(int postId, Comment value, int getUserId);
        ActionResult<Comment> UpdateComment(int postId, int id, Comment value, int getUserId);
        ActionResult DeleteComment(int postId, int id, int getUserId);
    }
    
    public class PostService : IPostService
    {
        private readonly AppSettings _appSettings;
        private readonly IRepository<Inquiry> _inquiryRepository;
        private readonly IRepository<Post> _postRepository;
        private readonly IRepository<User> _userRepository;

        public PostService(IOptions<AppSettings> appSettings, IRepository<Post> postRepository, IRepository<User> userRepository, IRepository<Inquiry> inquiryRepository)
        {
            _appSettings = appSettings.Value;
            _postRepository = postRepository;
            _userRepository = userRepository;
            _inquiryRepository = inquiryRepository;
        }
        
        public IEnumerable<Post> GetAll(int? id)
        {
            if (id == null)
            {
                return _postRepository.GetAll().Where(x => !_inquiryRepository.GetAll().Where(i => i.Post.Id == x.Id).Any(y => y.Accepted));
            }
            return _postRepository.GetAll().Where(x => !_inquiryRepository.GetAll().Where(i => i.Post?.Id == x.Id).Any(y => y.Accepted)).Select(x =>
            {
                x.SentInquiryId = _inquiryRepository.GetAll()
                    .FirstOrDefault(y => y.Post?.Id == x.Id && y.CreatedBy?.Id == id)?.Id;
                return x;
            }).ToList();
        }

        public Post GetPost(int id)
        {
            return _postRepository.Get(id); ;
        }

        public Post Add(Post post, int userId)
        {
            post.CreatedBy = _userRepository.Get(userId);

            if (post.CreatedBy == null)
            {
                return null;
            }
            
            _postRepository.Add(post);
            return post;
        }

        public ActionResult<Comment> AddComment(int postId, Comment value, int getUserId)
        {
            var post = GetPost(postId);
            if (post == null)
            {
                return new NotFoundResult();
            }

            value.CreatedBy = _userRepository.Get(getUserId);
            if (post.Comments == null)
            {
                post.Comments = new List<Comment>();
            }
            post.Comments.Add(value);
            
            _postRepository.Update(post, post);
            return value;
        }

        public ActionResult<Comment> UpdateComment(int postId, int id, Comment value, int getUserId)
        {
            var post = GetPost(postId);
            var comment = post?.Comments?.FirstOrDefault(x => x.Id == id);
            if (comment == null)
            {
                return new NotFoundResult();
            }

            if (comment.CreatedBy.Id != getUserId && getUserId != 0)
            {
                return new UnauthorizedResult();
            }

            comment.Text = value.Text;
            _postRepository.Update(post, post);
            return comment;
        }

        public ActionResult DeleteComment(int postId, int id, int getUserId)
        {
            var post = GetPost(postId);
            var comment = post?.Comments?.FirstOrDefault(x => x.Id == id);
            if (comment == null)
            {
                return new OkResult();
            }

            if (comment.CreatedBy.Id != getUserId && getUserId != 0)
            {
                return new UnauthorizedResult();
            }

            post.Comments.Remove(comment);
            _postRepository.Update(post, post);
            return new OkResult();
        }

        public ActionResult<Post> Update(int id, Post post, int userId)
        {
            var dbPost = GetPost(id);
            if (dbPost.CreatedBy.Id != userId && userId != 0)
            {
                return new UnauthorizedResult();
            }
            _postRepository.Update(GetPost(id), post);
            return dbPost;
        }

        public void Delete(int id)
        {
            _postRepository.Delete(GetPost(id));
        }
    }
}