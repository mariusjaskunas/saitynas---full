﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Entities;
using Helpers;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repository;

namespace Services
{
    public interface IUserService
    {
        User Authenticate(string email, string password);
        IEnumerable<User> GetAll();
        User GetUser(int id);
        void Register(User user);
        User Update(int id, User user);
        void Delete(int id);
    }
    
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IRepository<User> _userRepository;

        public UserService(IOptions<AppSettings> appSettings, IRepository<User> userRepository)
        {
            _appSettings = appSettings.Value;
            _userRepository = userRepository;
        }

        public User GetUserByCredentials(string email, string password)
        {
            var user = _userRepository.GetAll().FirstOrDefault(x => x.Email == email);
            if (user == null)
            {
                return null;
            }

            (string password, string salt) hash = GetHashedPassword(password, user.Salt);

            return user.Password == hash.password ? GetUser(user.Id) : null;
        }
        
        public User Authenticate(string email, string password)
        {
            var user = GetUserByCredentials(email, password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;
            user.Salt = null;

            return user;
        }

        public void Register(User user)
        {
            (string password, string salt) hash = GetHashedPassword(user.Password);

            var newUser = new User
            {
                Email = user.Email,
                Username = user.Username,
                Password = hash.password,
                Role = Role.User,
                Salt = hash.salt
            };
            
            _userRepository.Add(newUser);
        }

        private (string hashedPassword, string salt) GetHashedPassword(string password, string salt = null)
        {
            StringBuilder generatedSalt = new StringBuilder(16);
            if (salt == null)
            {
                char[] chars =
                    "abcdefghijklmnopqrstuvwxxxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[16];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }
                foreach (byte b in data)
                {
                    generatedSalt.Append(chars[b % (chars.Length)]);
                }
            }
            else
            {
                generatedSalt = new StringBuilder(salt);
            }

            var saltBytes = Encoding.ASCII.GetBytes(generatedSalt.ToString());

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: saltBytes,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return (hashed, Encoding.ASCII.GetString(saltBytes));
        }
        
        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            return _userRepository.GetAll().Select(x => {
                x.Password = null;
                x.Salt = null;
                x.Token = null;
                return x;
            });
        }

        public User GetUser(int id)
        {
            var user = _userRepository.Get(id);
            user.Password = null;
            user.Salt = null;
            user.Token = null;
            return user;
        }

        public User Update(int id, User user)
        {
            _userRepository.Update(GetUser(id), user);
            return user;
        }

        public void Delete(int id)
        {
            _userRepository.Delete(GetUser(id));
        }
    }
}