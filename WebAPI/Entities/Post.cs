﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Entities
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] 
        public int Id { get; set; }
        
        [Required]
        public string Title { get; set; }
        [Required]
        public string MainText { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
        [JsonIgnore]
        public User CreatedBy { get; set; }
        [NotMapped]        
        public int? CreatedByUserId => CreatedBy?.Id;
        [NotMapped]  
        public string UserName => CreatedBy?.Username;
        [NotMapped]
        public int? SentInquiryId {get; set;}
        [NotMapped]
        public int? Accepted {get; set;}
        public List<Comment> Comments { get; set; }
        public int Visits { get; set; }
    }
}