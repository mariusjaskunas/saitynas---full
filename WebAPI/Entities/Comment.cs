﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Entities
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
        public List<Comment> SubComments { get; set; }
        public Comment ParentComment { get; set; }
        [JsonIgnore]
        public User CreatedBy { get; set; }
        [NotMapped]  
        public int? CreatedByUserId => CreatedBy?.Id;
        [NotMapped]  
        public string UserName => CreatedBy?.Username;
    }
}