﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Entities
{
    public class InquiryMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
        [JsonIgnore]
        public Inquiry Inquiry { get; set; }
        [JsonIgnore]
        public User CreatedBy { get; set; }
        [NotMapped]
        public int? CreatedByUserId => CreatedBy?.Id;
        [NotMapped]  
        public string UserName => CreatedBy?.Username;
    }
}