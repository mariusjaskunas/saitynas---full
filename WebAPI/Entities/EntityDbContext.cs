﻿using Microsoft.EntityFrameworkCore;

namespace Entities
{
    public class EntityDbContext : DbContext
    {
        public EntityDbContext(DbContextOptions options)
            : base(options)
        {
        }
 
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Inquiry> Inquiries { get; set; }
        public DbSet<InquiryMessage> InquiryMessages { get; set; }
        public DbSet<InquiryReview> InquiryReviews { get; set; }
        public DbSet<Comment> Comments { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 1,
                Username = "UncleBob",
                Role = Role.Admin,
                Password = "test123",
                Email = "uncle.bob@gmail.com",

            }, new User
            {
                Id = 2,
                Username = "Kid",
                Role = Role.User,
                Password = "test123",
                Email = "kid@gmail.com",
            });
        }
    }
}