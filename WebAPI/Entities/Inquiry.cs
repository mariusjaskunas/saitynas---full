﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Entities
{
    public class Inquiry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
        [JsonIgnore]
        public User CreatedBy { get; set; }
        [NotMapped]
        public int? CreatedByUserId => CreatedBy?.Id;
        [NotMapped]  
        public string UserName => CreatedBy?.Username;
        public Post Post { get; set; }
        public List<InquiryMessage> Messages { get; set; }
        public bool Accepted { get; set; }
        public bool Completed { get; set; }
        public InquiryReview InquiryReview { get; set; }
    }
}