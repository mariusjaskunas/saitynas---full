﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class InquiryRepository : IRepository<Inquiry>
    {
        readonly EntityDbContext _dbContext;
 
        public InquiryRepository(EntityDbContext context)
        {
            _dbContext = context;
        }
 
        public IEnumerable<Inquiry> GetAll()
        {
            return _dbContext.Inquiries
                .Include(x => x.CreatedBy)
                .Include(x => x.Post)
                .ToList();
        }
 
        public Inquiry Get(int id)
        {
            return _dbContext.Inquiries
                .Include(x => x.CreatedBy)
                .Include(x => x.Post).ThenInclude(x => x.CreatedBy)
                .Include(x => x.Messages)
                .Include(x => x.CreatedBy)
                .Include(x => x.InquiryReview)
                .FirstOrDefault(e => e.Id == id);
        }

        public void Add(Inquiry entity)
        {
            entity.CreatedBy = _dbContext.Users.FirstOrDefault(e => e.Id == entity.CreatedBy.Id);
            entity.Post = _dbContext.Posts.FirstOrDefault(x => x.Id == entity.Post.Id);
            _dbContext.Inquiries.Add(entity);
            _dbContext.SaveChanges();
        }
 
        public void Update(Inquiry inquiry, Inquiry entity)
        {
            inquiry.Text = entity.Text;
            inquiry.Accepted = entity.Accepted;
            inquiry.Completed = entity.Completed;
            inquiry.InquiryReview = entity.InquiryReview;

            _dbContext.SaveChanges();
        }
        
        public void AddMessage(Inquiry inquiry, InquiryMessage entity)
        {
            inquiry.Messages.Add(entity);
            _dbContext.SaveChanges();
        }
        
        public void RemoveMessage(Inquiry inquiry, InquiryMessage entity)
        {
            inquiry.Messages.Remove(entity);
            _dbContext.SaveChanges();
        }
        
        public void UpdateMessage(Inquiry inquiry, InquiryMessage entity)
        {
            var message = inquiry.Messages.FirstOrDefault(x => x.Id == entity.Id);

            if (message != null)
            {
                message.Text = entity.Text;
                _dbContext.SaveChanges();
            }
        }
 
        public void Delete(Inquiry post)
        {
            _dbContext.Inquiries.Remove(post);
            _dbContext.SaveChanges();
        }
    }
}