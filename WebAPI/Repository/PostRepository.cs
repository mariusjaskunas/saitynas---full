﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class PostRepository : IRepository<Post>
    {
        readonly EntityDbContext _dbContext;
 
        public PostRepository(EntityDbContext context)
        {
            _dbContext = context;
        }
 
        public IEnumerable<Post> GetAll()
        {
            return _dbContext.Posts    
                .Include(x => x.CreatedBy)
                .Include(x => x.Comments).ThenInclude(x => x.CreatedBy)
                .ToList();
        }
 
        public Post Get(int id)
        {
            return _dbContext.Posts
                .Include(x => x.Comments).ThenInclude(x => x.CreatedBy)
                .Include(x => x.CreatedBy)
                .FirstOrDefault(e => e.Id == id);
        }

        public void Add(Post entity)
        {
            entity.CreateDateTime = DateTime.Now;
            entity.UpdateDateTime = DateTime.Now;
            entity.CreatedBy = _dbContext.Users.FirstOrDefault(e => e.Id == entity.CreatedBy.Id);
            _dbContext.Posts.Add(entity);
            _dbContext.SaveChanges();
        }
 
        public void Update(Post post, Post entity)
        {
            post.Title = entity.Title;
            post.MainText = entity.MainText;
            post.UpdateDateTime = DateTime.Now;
            post.Visits = entity.Visits;
            post.Comments = entity.Comments;

            _dbContext.SaveChanges();
        }
 
        public void Delete(Post post)
        {
            _dbContext.Posts.Remove(post);
            _dbContext.SaveChanges();
        }
    }
}