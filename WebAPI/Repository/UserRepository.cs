﻿using System.Collections.Generic;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class UserRepository : IRepository<User>
    {
        readonly EntityDbContext _dbContext;
 
        public UserRepository(EntityDbContext context)
        {
            _dbContext = context;
        }
 
        public IEnumerable<User> GetAll()
        {
            return _dbContext.Users
                .ToList();
        }
 
        public User Get(int id)
        {
            return _dbContext.Users
                .FirstOrDefault(e => e.Id == id);
        }

        public void Add(User entity)
        {
            _dbContext.Users.Add(entity);
            _dbContext.SaveChanges();
        }
 
        public void Update(User user, User entity)
        {
            user.Username = entity.Username;
            user.Role = entity.Role;
            user.Email = entity.Email;

            _dbContext.SaveChanges();
        }
 
        public void Delete(User user)
        {
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }
    }
}