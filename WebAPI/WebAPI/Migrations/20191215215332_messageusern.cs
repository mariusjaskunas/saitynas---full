﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class messageusern : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreatedById",
                table: "InquiryMessages",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_InquiryMessages_CreatedById",
                table: "InquiryMessages",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_InquiryMessages_Users_CreatedById",
                table: "InquiryMessages",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InquiryMessages_Users_CreatedById",
                table: "InquiryMessages");

            migrationBuilder.DropIndex(
                name: "IX_InquiryMessages_CreatedById",
                table: "InquiryMessages");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "InquiryMessages");
        }
    }
}
