﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebAPI.Controllers
{
    [Route("api/posts")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostService _postService;
        
        public PostsController(IPostService postService)
        {
            _postService = postService;
        }
        
        [HttpGet]
        public IEnumerable<Post> Get()
        {
            return _postService.GetAll(null);
        }
        
        [HttpGet("/api/posts-authorized")]
        [Authorize]
        public async Task<IEnumerable<Post>> GetAuthorized()
        {
            var userId = await Helpers.GetUserId(HttpContext);
            return _postService.GetAll(userId);
        }

        [HttpGet("{id}")]
        public ActionResult<Post> Get(int id)
        {
            return _postService.GetPost(id);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Post>> Post([FromBody] Post value)
        {
            var userId = (await Helpers.GetUserId(HttpContext)).Value;

            return _postService.Add(value, userId) != null
                ? (ActionResult<Post>) StatusCode(StatusCodes.Status201Created)
                : BadRequest(new {message = "Failed to add new Post"});
        }
        
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<Post>> Put(int id, [FromBody] Post value)
        {
            var userId = (await Helpers.GetUserId(HttpContext)).Value;
            return _postService.Update(id, value, userId);
        }
        
        [HttpDelete("{id}")]
        [Authorize]
        public void Delete(int id)
        {
            _postService.Delete(id);
        }
    }
}