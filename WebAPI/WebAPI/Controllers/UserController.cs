﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Security.Claims;
 using System.Threading.Tasks;
 using Entities;
 using Microsoft.AspNetCore.Authentication;
 using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Http;
 using Microsoft.AspNetCore.Mvc;
 using Microsoft.IdentityModel.Tokens;
 using Services;

 namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpGet]
        [Authorize(Roles = Role.Admin)]
        public ActionResult<IEnumerable<User>> Get()
        {
            return new ActionResult<IEnumerable<User>>(_userService.GetAll());
        }
        
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            return _userService.GetUser(id);
        }
        
        [AllowAnonymous]
        [HttpPut("login")]
        public IActionResult Login(User userCredentials)
        {
            var user = _userService.Authenticate(userCredentials.Email, userCredentials.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult<User> Register([FromBody] User user)
        {
            _userService.Register(user);
            return _userService.Authenticate(user.Email, user.Password);
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Put(int id, [FromBody] User value)
        {
            var userId = (await HttpContext.AuthenticateAsync()).Principal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            if (id.ToString() != userId)
            {
                Unauthorized();
            }
            var user = _userService.Update(id, value);
            return user;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var userId = (await HttpContext.AuthenticateAsync()).Principal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            if (id.ToString() != userId)
            {
                return Forbid();
            }

            _userService.Delete(id);
            return StatusCode(StatusCodes.Status200OK);
        }
    }
}