﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebAPI.Controllers
{
    [Route("api/inquiries/{inquiryId}/messages")]
    [ApiController]
    [Authorize]
    public class InquiryMessageController : ControllerBase
    {
        private readonly IInquiryService _inquiryService;
        
        public InquiryMessageController(IInquiryService inquiryService)
        {
            _inquiryService = inquiryService;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InquiryMessage>>> Get(int inquiryId)
        {
            return _inquiryService.GetMessages(inquiryId, (await Helpers.GetUserId(HttpContext)).Value);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<InquiryMessage>> Get(int inquiryId, int id)
        {
            return _inquiryService.GetMessage(inquiryId, id, (await Helpers.GetUserId(HttpContext)).Value);
        }
        
        [HttpPost]
        public async Task<ActionResult<InquiryMessage>> Post(int inquiryId, [FromBody] InquiryMessage value)
        {
            return _inquiryService.AddMessage(inquiryId, value, (await Helpers.GetUserId(HttpContext)).Value);
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<InquiryMessage>> Put(int inquiryId, int id, [FromBody] InquiryMessage value)
        {
            return _inquiryService.UpdateMessage(inquiryId, id, value, (await Helpers.GetUserId(HttpContext)).Value);
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int inquiryId, int id)
        {
            return _inquiryService.DeleteMessage(inquiryId, id, (await Helpers.GetUserId(HttpContext)).Value);
        }
    }
}