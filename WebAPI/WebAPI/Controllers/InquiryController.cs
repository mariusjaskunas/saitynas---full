﻿﻿using System.Collections.Generic;
 using System.Threading.Tasks;
 using Entities;
 using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Mvc;
 using Services;

 namespace WebAPI.Controllers
{
    [Route("api/inquiries")]
    [ApiController]
    [Authorize]
    public class InquiryController : ControllerBase
    {
        private readonly IInquiryService _inquiryService;
        
        public InquiryController(IInquiryService inquiryService)
        {
            _inquiryService = inquiryService;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Inquiry>> Get()
        {
            return _inquiryService.GetAll((await Helpers.GetUserId(HttpContext)).Value);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Inquiry>> Get(int id)
        {
            var inquiry = _inquiryService.Get(id, (await Helpers.GetUserId(HttpContext)).Value);
            return inquiry;
        }
        
        [HttpPost]
        public async Task<ActionResult<Inquiry>> Post([FromBody] Inquiry value)
        {
            var inquiry = _inquiryService.Add(value,(await Helpers.GetUserId(HttpContext)).Value);
            return inquiry;
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Inquiry>> Put(int id, [FromBody] Inquiry value)
        {
            var inquiry = _inquiryService.Update(id, value, (await Helpers.GetUserId(HttpContext)).Value);
            return inquiry;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var inquiry = _inquiryService.Delete(id, (await Helpers.GetUserId(HttpContext)).Value);
            return inquiry;
        }
    }
}