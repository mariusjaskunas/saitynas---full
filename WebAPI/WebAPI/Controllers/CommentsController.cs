﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using Entities;
 using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Mvc;
 using Services;

 namespace WebAPI.Controllers
{
    [Route("api/posts/{postId}/comments")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly IPostService _postService;
        
        public CommentsController(IPostService postService)
        {
            _postService = postService;
        }
        
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Comment>> Get(int postId)
        {
            return _postService.GetPost(postId).Comments;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Comment> Get(int postId, int id)
        {
            return _postService.GetPost(postId).Comments.FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Comment>> Post(int postId, [FromBody] Comment value)
        {
            return _postService.AddComment(postId, value, (await Helpers.GetUserId(HttpContext)).Value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<Comment>> Put(int postId, int id, [FromBody] Comment value)
        {
            return _postService.UpdateComment(postId, id, value, (await Helpers.GetUserId(HttpContext)).Value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> Delete(int postId, int id)
        {
            return _postService.DeleteComment(postId, id, (await Helpers.GetUserId(HttpContext)).Value);
        }
    }
}