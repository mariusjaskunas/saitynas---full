﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace WebAPI
{
    public static class Helpers
    {
        public static async Task<int?> GetUserId(HttpContext httpContext)
        {
            var userIdString = (await httpContext.AuthenticateAsync())?.Principal?.Claims?.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            var role = (await httpContext.AuthenticateAsync())?.Principal?.Claims?.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;
            
            if (userIdString == null)
            {
                return null;
            }


            if (role == "Admin")
            {
                return 0;
            }
            
            int.TryParse(userIdString, out int result);

            return result;
        }
    }
}